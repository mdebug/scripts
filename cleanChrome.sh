#backup current cache and cookies
mkdir -p ~/.google/chrome/backup
mv ~/.config/google-chrome/Default/ ~/.google/chrome/backup
mv ~/.cache/google-chrome ~/.google/chrome/backup

#clean chrome browser cache
rm ~/.config/google-chrome/Default/
rm ~/.cache/google-chrome