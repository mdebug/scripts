mkdir -p ~/.mozilla/firefox/backup ~/.cache/mozilla/firefox/backup
mv ~/.mozilla/firefox/*.default/*.sqlite  ~/.mozilla/firefox/backup
mv ~/.mozilla/firefox/*.default/sessionstore.js ~/.mozilla/firefox/backup
mv ~/.cache/mozilla/firefox/*.default/* ~/.cache/mozilla/firefox/backup

#clean cookies
rm ~/.mozilla/firefox/*.default/cookies.sqlite

#clean whole browsing cache
rm ~/.mozilla/firefox/*.default/*.sqlite ~/.mozilla/firefox/*default/sessionstore.js
rm -r ~/.cache/mozilla/firefox/*.default/*

