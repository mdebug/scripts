#!/bin/bash

process="$1"

foo(){
    local f=$1
    if [ "$f" ]
    then
        retval="checking for process $f"
    else
        echo "No Arguments"
    fi
}

retval = "nothing"
foo me
echo "Returned $retval"
foo two
echo "Returned $retval"

foo "$process"
echo "Returned $retval"



